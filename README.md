# JDeploy

This is a simple tool that works together with the Jenkins [HTTP Post plugin](https://wiki.jenkins-ci.org/display/JENKINS/HTTP+POST+Plugin).

The upload script processes artifacts posted by Jenkins and stores the files on disk in a *$job-name/$build-num* structure with symlinks pointing to the previous and latest builds together with md5 checksums.


## Configuration

The *$CONFIG['storage']* path defined in the config.php file must be writable by the user running the webserver (www-data in case of Debian/Ubuntu).


## Security

Configure your webserver to do authentication, limit GET / POST or whatever requirements you may have.


## Usage

### Upload

In Jenkins you have to install the *HTTP Post* plugin and configure it to POST to the URL where you host the upload script.

You have to configure one or more Jobs in Jenkins for which you want to receive artifacts from. Do this by configuring Jenkins to archive artifacts and enable the *HTTP Post artifacts to an URL* Post-build action for the Jobs in question.


### Download

The download script expects a *job* parameter and one of *build* or *tag*. If no *file* is provided, you will se a list of artifacts, and if *file* is provided, the content of the file is returned. 
