<?php

# We keep our file structure within this path
$CONFIG['storage'] = "/srv/dav/jenkins";

# Limit upload to a maximum of 50MB (50 * 1024 * 1024)
$CONFIG['maxsize'] = 52428800;


# Load local configuration if present
if (file_exists(dirname(__FILE__).'/config.local.php'))
  include_once 'config.local.php';
