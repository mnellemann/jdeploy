<?php
/*
 * (C) Copyright 2015 Mark Nellemann <mark.nellemann@gmail.com> and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     ...
 */

require_once 'config.php';

# Relevant GET params
$job_name = (empty($_GET['job'])) ? NULL : $_GET['job'];
$tag_name = (empty($_GET['tag'])) ? NULL : $_GET['tag'];
$build_no = (empty($_GET['build'])) ? NULL : $_GET['build'];
$file_name = (empty($_GET['file'])) ? NULL : $_GET['file'];

# Storage locations
global $source;
$source['dir'] = NULL;
$source['top'] = sprintf("%s/%s", $CONFIG['storage'], $job_name);
$source['tag'] = sprintf("%s/%s/%s", $CONFIG['storage'], $job_name, $tag_name);
$source['build'] = sprintf("%s/%s/%s", $CONFIG['storage'], $job_name, $build_no);


try {

  # Check job name
  if(!preg_match('/^[a-zA-Z0-9_-]{3,}$/i', $job_name)) {
    throw new RuntimeException("Invalid job: $job_name");
  }

  # Check job dir
  if(!is_dir($source['top'])) {
    throw new RuntimeException("Unknown job: $job_name");
  }

  # Check build number
  if( $build_no !== NULL && 1 !== preg_match('/^[0-9]+$/', $build_no) ) {
    throw new RuntimeException("Invalid build: $build_no");
  }

  # Check build dir
  if( $build_no && !is_dir($source['build'])) {
    throw new RuntimeException("Unknown build: $build_no");
  } elseif ($build_no) {
    $source['dir'] = $source['build'];
  }

  # Check tag name
  if( $tag_name !== NULL && !preg_match('/^[a-zA-Z0-9_-]{3,}$/i', $tag_name) ) {
    throw new RuntimeException("Invalid tag: $tag_name");
  }

  # Check tag dir
  if( $tag_name && !is_link($source['tag'])) {
    throw new RuntimeException("Unknown tag: $tag_name");
  } elseif ($tag_name) {
    $source['dir'] = sprintf("%s/%s", $source['top'], readlink($source['tag']));
  }

  # Build and tag are mutually exclusive, but either is required
  if($build_no && $tag_name) {
    throw new RuntimeException("Build and Tag are mutually exclusive.");
  }
  if(!$build_no && !$tag_name) {
    throw new RuntimeException("Neither build nor tag is provided.");
  }

  if(!$file_name) {

    # Read and print dir content
    $files = scandir($source['dir']);
    foreach($files as $file) {
      if(!preg_match('/^(\.)+$/', $file)) {
        header('Content-Type: text/plain; charset=utf-8');
        echo sprintf("%s\n", $file);
      }
    }

  } else {

    # Check file name
    if(1 !== preg_match('/^[a-zA-Z0-9_-]{3,}(\.[a-zA-Z0-9]{1,3})?$/', $file_name)) {
      throw new RuntimeException("Invalid file.");
    }

    # Make sure file exist
    $file = sprintf("%s/%s", $source['dir'], $file_name);
    if(!is_file($file)) {
      throw new RuntimeException("Unknown file.");
    }

    # Send file content to client
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;

  }

} catch (RuntimeException $e) {

  header('Content-Type: text/plain; charset=utf-8');
  echo $e->getMessage();

}
