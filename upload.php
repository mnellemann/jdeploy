<?php
/*
 * (C) Copyright 2015 Mark Nellemann <mark.nellemann@gmail.com> and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     ...
 */

require_once 'config.php';

# Relevant HTTP headers
$job_name = $_SERVER['HTTP_JOB_NAME'];
$build_no = $_SERVER['HTTP_BUILD_NUMBER'];
$build_ts = $_SERVER['HTTP_BUILD_TIMESTAMP'];

# Storage locations
$destination_prev = sprintf("%s/%s/previous", $CONFIG['storage'], $job_name);
$destination_link = sprintf("%s/%s/latest", $CONFIG['storage'], $job_name);
$destination_path = sprintf("%s/%s/%s", $CONFIG['storage'], $job_name, $build_no);

header('Content-Type: text/plain; charset=utf-8');

# Process artifacts
foreach($_FILES as $file) {

  echo "Processing Artifact: " . $file['name'] . "\n";
  $destination_file = sprintf("%s/%s", $destination_path, $file['name']);
  $destination_md5 = sprintf("%s.md5", $destination_file);

  try {

    // Detect if build exists - We don't want to overwrite
    if(is_dir($destination_path)) {
      throw new RuntimeException('Build exists.');
    }

    // Limit filesize of uploaded artifacts
    if ($file['size'] > $CONFIG['maxsize']) {
      throw new RuntimeException('Exceeded filesize limit.');
    }

    // Create directory structure if missing
    if ( ! is_dir($destination_path)) {
      if(! mkdir($destination_path, 0755, TRUE)) {
        throw new RuntimeException("Failed to create dir $destination_path");
      }
    }

    // Move uploaded tmp file into final distination
    if (!move_uploaded_file($file['tmp_name'], $destination_file)) {
      throw new RuntimeException("Failed to move uploaded file to $destination_file");
    }

    // Create a md5sum of the uploaded file
    $md5sum = md5_file($destination_file);
    if(!file_put_contents($destination_md5, $md5sum)) {
      throw new RuntimeException("Failed to save md5 checksum into $destination_md5");
    }

  } catch (RuntimeException $e) {

    echo $e->getMessage();

  }

}


# Manage symbolic links
if(sizeof($_FILES) > 0) {

  try {

    # Delete symlink for previous build
    if(is_link($destination_prev)) {
      if(!unlink($destination_prev)) {
        throw new RuntimeException("Failed to remove old 'previous' symlink $destination_prev");
      }
    }

    # Delete symlink for latest build
    if(is_link($destination_link)) {

      $old_target = readlink($destination_link);
      if(!unlink($destination_link)) {
        throw new RuntimeException("Failed to remove old 'latest' symlink $destination_link");
      }

      # Create new symlink for previous build
      if(!symlink($old_target, $destination_prev)) {
        throw new RuntimeException("Failed to symlink $destination_prev to $old_target");
      }
    }

    # Create new symlink for latest build
    if(!symlink($destination_path, $destination_link)) {
      throw new RuntimeException("Failed to symlink $destination_link to $destination_path");
    }

  } catch (RuntimeException $e) {

    echo $e->getMessage();

  }

}
